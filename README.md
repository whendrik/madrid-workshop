# Madrid Housing Workshop on the IBM Cloud

## Get Familiar with the IBM Cloud

The IBM Cloud has different views, depending on your role;

* [cloud.ibm.com](https://cloud.ibm.com/) - The main site, all services and functionality can be reached from here. Suited for Developers (e.g. create web applications). 
* [dataplatform.cloud.ibm.com](https://dataplatform.cloud.ibm.com/) - The Cloud Pak for Data site, all data related services can be reached from here. Suited for Data Scientists (e.g. create sklearn models).

## 1 - Login the IBM Cloud, `cloud.ibm.com`

Login to [cloud.ibm.com](https://cloud.ibm.com/). Possible you already have some services provisioned.

![cloud.ibm.com](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/cloud.ibm.com.png)

## 2 - Login the IBM Cloud pak for Data, `dataplatform.cloud.ibm.com`

Login to [dataplatform.cloud.ibm.com](https://dataplatform.cloud.ibm.com/). The first time you logon some services are created.

![dataplatform.cloud.ibm.com](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/dataplatform.cloud.ibm.com.gif)

---

Notice that on [cloud.ibm.com](https://cloud.ibm.com/) indeed services have been created. 

![check_services](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/check_services.gif)


## 3 - Create your first Project

Create your first project. Notice that it routes you to create Cloud Object Storage (COS) first. COS will store all data files and project information.

![1_create_project](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/1_create_project.gif)

## 4 - Load the necessary Data Files

Download the files from [Madrid Demo on Cloud](https://gitlab.com/whendrik/madrid-demo-on-cloud), and upload the following files to the project;

- `madrid_houses_v5.csv`
- `unique_metro_v5.csv`
- `autoai_v5.csv`

![2_add_data](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/2_add_data.gif)

## 5 - Load the Jupyter Notebook

Download the `Madrid Housing Demo.ipynb` notebook from [Madrid Demo on Cloud](https://gitlab.com/whendrik/madrid-demo-on-cloud), and;

1. Add to Project > **Notebook**
2. Choose **From File**

![3_add_notebook](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/3_add_notebook.gif)

## 6 - Run the Notebook

Read the instructions carefully, and go through the notebook. Some cells require you to take an action.

---

For instance, create a API Key

![create_api_key](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/create_api_key.gif)

---

For instance, create a deployment space

![create_deployment_space](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/create_deployment_space.gif)

---

At the end, you have a model deployed!

Want to see a working example using the deployed model?

on [madridhousingdeploymentdemo.mybluemix.net](https://madridhousingdeploymentdemo.mybluemix.net/) we hosted a Flask Application using the deployed model as a backend. The [code](https://gitlab.com/whendrik/madridhousingdeploymentdemo/) is available, it uses the cloud foundry framework.

## 7 - AutoAI

Want to use Automated Machine Learning to create a model for you?

Use the `autoai_v5.csv` for the AutoAI feature to automatically create a model for you, only thing you have to do is select `price` as the target.

![auto_ai](https://gitlab.com/whendrik/madrid-workshop/-/raw/master/images/auto_ai.gif)

## 8 - What did we **not** do today ?


- Deploy [Custom Functions](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-deployed-func-mnist-tutorial.html)

- Build a visual recognition app, with [Watson Visual Recognition](https://cloud.ibm.com/catalog/services/visual-recognition)

- Using [GPUs](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/gpu-environments.html)