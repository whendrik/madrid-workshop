# Quiz Questions

- **Environments** are docker images - you can `!pip install` inside them. Is that action persistent, i.e. will it be forever?

- **Environments** with CPLEX always charge an additional 20 Capacity Hours, why is that?

- **Environments** can be spawn and shutdown on the fly. What if you need to work with `pickle` or `.csv` files?

https://gitlab.com/whendrik/s3-boto-examples

- Guess where all the meta-data of the project is stored?

https://gitlab.com/whendrik/xgboost

- Looking at the Linear Regression plot, what do you notice? How is this phenomenon called?

- `XGBoost` : Say we want to encode a categorical feature like type-of-house, do we want `one-hot-encoding` or `label-encoding` with XGBoost?

- `XGBoost` : Sometimes, XGboost with trees as weak learning (and other tree based models like Random Forest) suffer from prediction 'outside the forest', that is they don't generelize well when features are scales in such proportions they don't match training data well. What is a trick sometimes used, we can apply perfectly here with square-m ?